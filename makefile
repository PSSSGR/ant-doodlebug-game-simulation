#/*
 #* Filename:	Makefile
 #* Name:		Sri Padala
 #* Description:	Ant Doodlebug SIMULATION
#/
CXX=g++
CXXFLAGS=-Wall -std=c++11
EXE=hw06

SRCDIR = src
reldir = rel
dbgdir = dbg
SRCS = main.cpp Ant.cpp  simulation.cpp  Critter.cpp  DoodleBug.cpp

OBJS=$(SRCS:%.cpp=%.o)
relOBJS:=$(addprefix $(reldir)/,$(OBJS))
debugOBJS:=$(addprefix $(dbgdir)/,$(OBJS))

Debug: $(dbgdir)/$(EXE)
release: $(reldir)/$(EXE)

.PHONY : prep release debug clean

prep:
	@mkdir dbg rel > /dev/null 2>& 1; true

$(reldir)/$(EXE): $(relOBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(reldir)/%.o:$(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(dbgdir)/$(EXE): $(debugOBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

$(dbgdir)/%.o:$(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -g -O0 -c -o $@ $<

clean :
	rm	-f $(OBJS) $(relOBJS) $(debugOBJS)  core $(dbgdir)/$(EXE) $(reldir)/$(EXE)
