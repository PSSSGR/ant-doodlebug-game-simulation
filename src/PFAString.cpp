/*
 * Filename:	PFAString.cpp 
 * Name:		Sri Padala
 * Description:	This program is about implementing a weak version of the vector class, which will be a dynamic array of string variables.
 */

#include "PFAString.hpp"
#include <string>
#include <iostream>

PFAString::PFAString() : _capacity(0), _size(0)
{
	arr = new std::string[_capacity];
}

PFAString::PFAString(int cap) : _capacity(cap), _size(cap)
{
	arr = new std::string[_capacity];
}

PFAString::PFAString(int cap, const std::string& str) : _capacity(cap), _size(cap)
{
	arr = new std::string[_capacity];
	for (int i = 0; i<_size; i++)
		arr[i] = str;
}

PFAString::PFAString(const PFAString& stringObject) : _capacity(stringObject._size), _size(stringObject._size)
{
  this->arr = new std::string[_size];
	for (int i = 0; i<_size; i++){
	   arr[i] = stringObject.arr[i];//Copies the contents of the string Object.
    }
}

PFAString::PFAString(PFAString&&  PFAStringObject) : _capacity(PFAStringObject._capacity), _size(PFAStringObject._size)
{
    this->arr = new std::string[_capacity];
	arr = PFAStringObject.arr;//tranfers the ownership.
	PFAStringObject.arr=nullptr;//makes the object point to null.
}

PFAString::~PFAString()
{
    	delete [] arr ;	
        arr=nullptr;
}

int PFAString::capacity() const
{
	return _capacity;
}
int PFAString::size() const
{
	return _size;
}

void PFAString::push_back(std::string item)
{
  // if space is full, allocate more capacity
  if (_size == _capacity)
    {
      if (_capacity == 0)
        // if capacity is 0, set capacity to 1.
        // set copy to false because there are
        // no existing elements
         reserve(1,false);
      else
        // double the capacity
        reserve(2 * _capacity, true);
    }
  
  // add item to the list, update vSize
  arr[_size] = item;
  _size++;
}

void PFAString::pop_back()   
{
    if(_size<=0){
		_size=0;
    }else{
		_size--;
    }

}

void PFAString::reserve(int n, bool copy)
{
  std::string *newArr;
  int i;
  
  // allocate a new dynamic array with n elements
  newArr = new std::string[n];
  if (newArr == NULL)
    // throw memoryAllocationError(
    std::cout<<"miniVector reserve(): memory allocation failure";
  
  // if copy is true, copy elements from the old list to the new list
  if (copy)
    for(i = 0; i < _size; i++)
      newArr[i] = arr[i];
  
  // delete original dynamic array. if vArr is NULL, the vector was
  // originally empty and there is no memory to delete
  if (arr != NULL)
    delete [] arr;
  
  // set vArr to the value newArr. update vCapacity
  arr = newArr;
  _capacity = n;
}
void PFAString::resize(int nsize) //shrink
{
  if(_capacity < nsize)
      grow();
	_size = nsize;
	
	if (_size < 0)
		_size = 0;
}
void PFAString::resize(int nsize, std::string tempStr) //grow
{
	_capacity = nsize;
	std::string* tempArr = new std::string[_capacity];
	for (int i = 0; i < _size; i++)
		tempArr[i] = arr[i];
	for (; _size < nsize; _size++)
		tempArr[_size] = tempStr;
	delete[] arr;
	arr = tempArr;
}

void PFAString::empty_array()        
    {
    	_size = 0 ;
    }

inline void PFAString::grow()
{
	if (_capacity == 0)
		_capacity = 1;
	for(;_capacity <= _size; _capacity = _capacity * 2);
	    std::string* tempArr = new std::string[_capacity];
	for (int i = 0; i < _size; i++)
		tempArr[i] = arr[i];
	delete[] arr;
	arr = tempArr;
}

PFAString& PFAString:: operator =(const PFAString& object) //Copy Constructor
{
	delete[] arr;
	std::string* tempArr = new std::string[object._capacity];
	arr = tempArr;
	_size = object._size;
	_capacity = object._size;
	for (int i = 0; i < object._size; i++)
		arr[i] = object.arr[i];

	return *this;
}

PFAString& PFAString:: operator = (PFAString&& rSide)//Move constructor
{
    delete[] arr;
	std::string* tempArr = new std::string[rSide._capacity];
	arr = tempArr;
	_size = rSide._size;
	_capacity = rSide._capacity;
    arr = rSide.arr;
    rSide.arr=nullptr;//tranfers the ownership
	return *this;
}

std::string & PFAString::operator [](int pos)
{
	if (pos>=_capacity){
		exit(EXIT_FAILURE);
	}
	return(arr[pos]);
}
bool PFAString::operator ==(const PFAString& object) const
{
	if (_size == object.size())
	{
		for (int i = 0; i < _size; i++)
		{
			if (this->arr[i] != object.arr[i])
				return false;
		}
        return true;
	}else{
        return false;
    }	
}
bool PFAString::operator != (const PFAString& object) const
    {
    	if(*this == object){
            return false;
        }else{
            return true;
        }
    }

bool PFAString::operator <(const PFAString& object) const
{
 	int flag=0;

	if (_size > object._size)
	{
		for(int i =0;i < object._size;i++){
			if (arr[i] > object.arr[i])
				return false;
			if (arr[i] == object.arr[i])
				flag++;
		}
		if(flag == object._size)
			return false;
		return true;
	}
	else if(_size == object._size)
	{
		for (int i = 0; i < _size; i++) {
			if (arr[i] > object.arr[i])
				return false;
			if (arr[i] == object.arr[i])
				flag++;
		}
		if(flag == _size)
			return false;
		return true;
	}
	else if (_size < object._size)
	{
		for(int i =0; i<_size;i++){
			if (arr[i] > object.arr[i])
				return false;
			if (arr[i] == object.arr[i])
				flag++;
		}
		if(flag == _size)
			return true;
		return true;
	}
	return 0;
}

bool PFAString::operator >(const PFAString& object) const
{
	return(object < *this);
}
bool PFAString::operator <=(const PFAString& object) const
{
	return(!(object < *this));
}
bool PFAString::operator >=(const PFAString& object) const
{
	return(!(*this < object));
}

