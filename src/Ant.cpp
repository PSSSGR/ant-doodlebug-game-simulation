/*
 * Filename:	Ant.cpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#include "simulation.hpp"
#include "Critter.hpp"
#include "Ant.hpp"


Ant::Ant(Simulation* set_simulation, int xcoord, int ycoord) : Critter(set_simulation,xcoord,ycoord)
{
	id = 'O';//id for ants.
	breedCounter = 3;
	moveflag= false;
}

void Ant::move()
{
	if (this->moveFlag() == true)
	{
		breedCounter--;
		simulation->setposition(x, y, this);//sets the path.
	}
}

void Ant::newbug(int x, int y)
{
  simulation->increment();
	simulation->initPlace(x, y, new Ant(this->simulation, x, y));//creates a new ant.
	breedCounter = 3;
}
