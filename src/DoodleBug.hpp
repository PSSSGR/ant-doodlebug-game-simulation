/*
 * Filename:	DoodleBug.hpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#ifndef DOODLEBUG
#define DOODLEBUG
#include "Critter.hpp"


class Simulation;

class Doodlebug : public Critter
{
public:
	Doodlebug(Simulation* set_simulation, int xcoord, int ycoord);
	~Doodlebug() {};
	void move() override;
	void newbug(int xcoord, int ycoord);//Creates new doodlebug.
private:
};

#endif
