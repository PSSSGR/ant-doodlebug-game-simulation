/*
 * Filename:	DoodleBug.cpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#include "simulation.hpp"
#include "Critter.hpp"
#include "DoodleBug.hpp"


Doodlebug::Doodlebug(Simulation* set_simulation, int xcoord, int ycoord) : Critter(set_simulation, xcoord, ycoord)
{
	id = 'X';
	breedCounter = 8;
	hungerCounter = 3;
	moveflag = false;
}
void Doodlebug::move()
{
	if (this->moveFlag() == true)
	{
		breedCounter--;
		hungerCounter--;
		simulation->hunt(x,y,this);//sets the path.
	}
}
void Doodlebug::newbug(int x, int y)
{
 simulation->increment2();
	simulation->initPlace(x, y, new Doodlebug(this->simulation, x, y));//creates a doodlebug.
	breedCounter = 8;
}
