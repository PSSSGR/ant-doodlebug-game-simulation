/*
 * Filename:	Critter.hpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#ifndef CRITTER
#define CRITTER

class Simulation;
class Critter
{
public:
	Critter(Simulation* set_simulation, int xcoord, int ycoord);
	virtual ~Critter() {};
	void setCoord(int xcoord, int ycoord) { x = xcoord, y = ycoord; }//sets the coordinates.
	virtual void move() = 0;
	bool moveFlag();
	char getID();//gets the id of the cells whether it is a ant or doodlebug
	void resetmove();
	void resetHunger() { hungerCounter = 3; }
	bool health();//checks the health status
	void bread();//chacks the breeding status
	virtual void newbug(int xcoord, int ycoord) = 0;//creates new bugs.
protected:
	char id;
	int hungerCounter;
	int breedCounter;
	bool moveflag;
    Simulation* simulation;
	int x, y;

private:
};

#endif
