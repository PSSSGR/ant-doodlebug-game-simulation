/*
 * Filename:	simulation.cpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
 */
#include "simulation.hpp"
#include "Critter.hpp"
#include "Ant.hpp"
#include <iostream>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <ctime>
#include "DoodleBug.hpp"

int Simulation::antEaten=0;//initializing the static variables of the simulation class.
int Simulation::breadAnts=0;
int Simulation::breadDoodlebug=0;
int Simulation::starvedDoodlebug=0;
int Simulation::totalAnt=0;
int Simulation::totalDoodlebug=0;

Simulation::Simulation()
{
	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			grid[x][y] = nullptr;
		}
	}
	populate();//Making sure that evrything is pointing to null before populating.
}

Simulation::~Simulation()
{
	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] != nullptr)
			{
				delete grid[x][y];//Deleting the cells.
				grid[x][y] = nullptr;
			}
		}
	}
}

void Simulation::populate()
{
	int x_coordinate, y_coordinate;
	for (int i = 0; i < 5; i++)
	{
		do
		{
			x_coordinate = rand() % worldsize;//Creating the random number.
			y_coordinate = rand() % worldsize;//Creating the random number.
		} while (grid[x_coordinate][y_coordinate] != nullptr);
		this->grid[x_coordinate][y_coordinate] = new Doodlebug(this, x_coordinate, y_coordinate);//Creating the doodle bug at the cell.
	}
	for (int i = 0; i < 100; i++)
	{
		do
		{
			x_coordinate = rand() % worldsize;//Creating the random number.
			y_coordinate = rand() % worldsize;
		} while (grid[x_coordinate][y_coordinate] != nullptr);
		this->grid[x_coordinate][y_coordinate] = new Ant(this, x_coordinate, y_coordinate);//Cresting the ant for the required cell.
	}
}

void Simulation::output()
{
	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] == nullptr)
			{
				std::cout << "-";//prints - if the cell is empty.
			}
			else
			{
				std::cout << grid[x][y]->getID();//Prints out the grid.
			}
		}
		std::cout << "\n";
	}
}
void Simulation::stepSimulation()
{
	movePhase('X');//For DoodleBugs

	movePhase('O');//For Ants

	endPhase();//Printing the summary.

	std::cout << "\nSummary\n";
	std::cout << "\nTotal number of ants Eaten: " << antEaten;
	std::cout << "\nTotal new ants from breeding: "<< breadAnts;
	std::cout << "\nTotal new doodlebugs from breeding: " << breadDoodlebug;
	std::cout << "\nTotal doodlebugs starved : " << starvedDoodlebug;
	std::cout << "\nTotal ants: "<< totalAnt;
	std::cout << "\nTotal doodlebugs: "<< totalDoodlebug << "\n";

	if(totalDoodlebug == 0 && totalAnt == 0)//If simualtion ends.
	{
	  std::cout <<"\nGame Over\n";
	  abort();
	}
	else if(totalDoodlebug == 0)//If all doodlebugs die.
	{
	  std::cout <<"\n No Doodlebugs\n";
	  abort();
	}else if (totalAnt == 0)
	{
	  std::cout <<"\nno ants\n";//If all ants die.
	  abort();
	}

  antEaten = 0;
  breadAnts= 0;
  breadDoodlebug = 0;
  starvedDoodlebug = 0;
  totalAnt = 0;
  totalDoodlebug = 0;
  std::cout << "\nPress Enter to Continue\n";
}
bool Simulation::findAnt(int x, int y)
{
	if (valid(x, y + 1) && grid[x][y + 1] != nullptr)
	{
		if (grid[x][y + 1]->getID() == 'O') {//Checking for the ant.
			coordinates.x = 0;
			coordinates.y = 1;
			return true;
		}
	}
	if (valid(x, y - 1) && grid[x][y - 1] != nullptr)
	{
		if (grid[x][y - 1]->getID() == 'O') {//Checking for the ant.
			coordinates.x = 0;
			coordinates.y = -1;
			return true;
		}
	}
	if (valid(x + 1, y) && grid[x + 1][y] != nullptr)
	{
		if (grid[x + 1][y]->getID() == 'O') {//Checking for the ant.
			coordinates.x = 1;
			coordinates.y = 0;
			return true;
		}
	}
	if (valid(x - 1, y) && grid[x - 1][y] != nullptr)
	{
		if (grid[x - 1][y]->getID() == 'O') {//Checking for the ant.
			coordinates.x = -1;
			coordinates.y = 0;
			return true;
		}
	}
	return false;
}

void Simulation::hunt(int x, int y, Critter *matrix)
{
	direction();
	if (findAnt(x, y))//if ant is found , its position is pointed null.
	{
	  this->antEaten++;
		matrix->resetHunger();
		grid[x + coordinates.x][y + coordinates.y] = nullptr;
		grid[x + coordinates.x][y + coordinates.y] = matrix;
		grid[x][y] = nullptr;
		matrix->setCoord(x + coordinates.x, y + coordinates.y);
	}
	else if (empty(x + coordinates.x, y + coordinates.y))
	{
		grid[x + coordinates.x][y + coordinates.y] = matrix;
		grid[x][y] = nullptr;
		matrix->setCoord(x + coordinates.x, y + coordinates.y);
	}
}

void Simulation::movePhase(char type)
{
  if(type == 'X')
  {
    std::cout<<"\nDOODLEBUG MOVEMENT\n";
  }
  if(type == 'O')
  {
    std::cout<<"\nANT MOVEMENT\n";
  }

	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] != nullptr)
			{
				if (grid[x][y]->getID() == type)
				{
					grid[x][y]->move();
				}
			}
		}
	}
	this->output();
}

void Simulation::endPhase()
{
	reset_move();
	starvation();
	breeding();
	std::cout<<"Breeding and/or Stravation";
	countBugs();
	this->output();
}
void Simulation::reset_move()
{
	for (int y = 0; y < worldsize; y++)//resets the grid.
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] != nullptr)
			{
				this->grid[x][y]->resetmove();
			}
		}
	}
}

void Simulation::direction()
{
	int path = rand() % 4; //For Direction.
	switch (path) {
	case 0: //North
		coordinates.x = 0;
		coordinates.y = 1;
		break;
	case 1://South
		coordinates.x = 0;
		coordinates.y = -1;
		break;
	case 2: //East
		coordinates.x = 1;
		coordinates.y = 0;
		break;
	case 3: //West
		coordinates.x = -1;
		coordinates.y = 0;
		break;
	}
}

bool::Simulation::valid(int x, int y)//returns true if valid location.
{
	if (y<worldsize && y>=0) {
		if (x<worldsize && x>=0) {
			return true;
		}
	}
	return false;
}

bool Simulation::empty(int x, int y)//returns true if empty spot.
{
	if (y<worldsize && y>=0) {
		if (x<worldsize && x>=0) {
			if (grid[x][y] == nullptr) {
				return true;
			}
		}
	}
	return false;
}

void Simulation::countBugs()
{
	int doodlebug_count = 0;//Initializing the doodlebug count.
	int ant_count = 0;//initializing the ant count.
	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] != nullptr)
			{
				if (grid[x][y]->getID() == 'O')//counts ants
					ant_count++;
				if (grid[x][y]->getID() == 'X')//counts doodlebugs
					doodlebug_count++;
			}
		}
	}
	totalAnt = ant_count;
	totalDoodlebug = doodlebug_count;
}

void Simulation::starvation()
{
	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] != nullptr && grid[x][y]->getID() == 'X')
			{
				if (grid[x][y]->health())//checks the health status.
				{
				  this->starvedDoodlebug++;//Doodle bug die.
					delete grid[x][y];
					grid[x][y] = nullptr;
				}
			}
		}
	}
}

void Simulation::breeding()
{
	for (int y = 0; y < worldsize; y++)
	{
		for (int x = 0; x < worldsize; x++)
		{
			if (grid[x][y] != nullptr)
			{
				this->grid[x][y]->bread();//respective cell breeds.
			}
		}
	}
}

void Simulation::increment()
{
    breadAnts++;//incremnts the breedants.
}
void Simulation::increment2()
{
    breadDoodlebug++;//increments the breaddoodlebugs.
}


void Simulation::setposition(int x, int y,Critter *matrix)
{
	direction();
	if (empty(x+coordinates.x, y + coordinates.y))//sets the position of the cells in the grid.
	{
		grid[x + coordinates.x][y + coordinates.y] = matrix;
		grid[x][y] = nullptr;
		matrix->setCoord(x + coordinates.x, y + coordinates.y);
	}
}

void Simulation::initPlace(int x, int y, Critter *matrix)
{
	grid[x][y] = matrix;//initial data.
}
