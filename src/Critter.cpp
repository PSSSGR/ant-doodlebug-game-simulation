/*
 * Filename:	Critter.cpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#include "simulation.hpp"
#include "Critter.hpp"



Critter::Critter(Simulation* set_simulation, int xcoord, int ycoord) :simulation(set_simulation), x(xcoord), y(ycoord)
	{ }


bool Critter::moveFlag()
{
	if (this->moveflag == false)
	{
		this->moveflag = true;
		return true;
	}
	return false;
}

char Critter::getID()
{
	return id;//gets the id of the cells whether it is a ant or doodlebug
}

void Critter::resetmove()
{
	this->moveflag = false;//sets the moveflag to false.
}

bool Critter::health()//checks the health status
{
	if (this->hungerCounter == -1)
	{
		return true;
	}
	return false;
}


void Critter::bread()//chacks the breeding status
{
	if (breedCounter == -1)//breads at valid location.
	{
		if (simulation->empty(x , y + 1))
		{
			newbug(x, y + 1);
		}
		else if (simulation->empty(x, y -1))
		{
			newbug(x, y - 1);
		}
		else if (simulation->empty(x + 1, y))
		{
			newbug(x+1, y);
		}
		else if (simulation->empty(x - 1 , y))
		{
			newbug(x - 1, y);
		}
		else
		{
		 breedCounter = 0;
		}
	}
}
