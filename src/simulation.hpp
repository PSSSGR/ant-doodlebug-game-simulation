/*
 * Filename:	simulation.hpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#ifndef SIMULATION_HPP
#define SIMULATION_HPP
#include "Critter.hpp"
#include "Ant.hpp"
#include "DoodleBug.hpp"


struct position
{
	int x;
	int y;
};

class Simulation {
    public:
        Simulation();
        ~Simulation();
        void populate();//create a grid with ants and doodlebugs
	    void output();//prints the grid to the screen.
	    void direction();//gives the direction to go.
	    bool empty(int x, int y);//returns the boolesn value regarding the spot on the grid.
	    bool valid(int x, int y);//returns whether the given spot is valid or not.
	    void countBugs();//Doodlebug counter.
	    void starvation();//starvation counter.
	    void breeding();//breeding counter.
		void stepSimulation();//gives summary regarding each phase of the grid.
	    void hunt(int x, int y, Critter *matrix);//Kills the ants on a valid spot on the grid.
	    bool findAnt(int x, int y);//returns true if there is an ant on a cell.
	    void movePhase(char type);//sets up the movement of animals.
	    void endPhase();//calculates the stuff at the end of a phase.
	    void reset_move();//Resets the move phase.
        void increment();//increments the member variables
        void increment2();//increments the member vsriables
	    void setposition(int xcoord, int ycoord, Critter* matrix);//sets the movement of animals.
	    void initPlace(int xcoord, int ycoord, Critter* matrix);//Initials the matrix.
    private:
        int worldsize=20;
        position coordinates;
	    Critter *grid[20][20];
		static int breadAnts;
        static int antEaten ;
        static int starvedDoodlebug ;
        static int breadDoodlebug ;
        static int totalAnt ;
        static int totalDoodlebug ;
};


#endif
