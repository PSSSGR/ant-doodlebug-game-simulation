/*
 * Filename:	Ant.hpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#ifndef ANT
#define ANT
#include "Critter.hpp"

class Simulation;
class Ant : public Critter
{
public:
	Ant(Simulation* set_simulation, int xcood, int ycoord);
	~Ant() {};
	void move() override;
	void newbug (int xcoord, int ycoord);//creates new ant at the specified cell.
private:
};

#endif
