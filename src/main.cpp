/*
 * Filename:	main.cpp
 * Name:		Sri Padala
 * Description:	Ant Doodlebug SIMULATION
  */
#include<iostream>
#include<ctime>
#include "simulation.hpp"


int main()
{
	srand(time(NULL));//planting the seed.
	Simulation matrix;
	matrix.output();
	char response;
	std::cout << "Press Enter to Continue";
	std::cin.get(response);//getting the response from the user.
	while (response == '\n')
	{
	  std::cout<<"endl";
		for (int i = 0; i < 20; i++){
			std::cout << "*";
		}
		matrix.stepSimulation();//setting up the simulation.
		std::cin.get(response);
	}
	return 0;
}
